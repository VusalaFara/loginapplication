FROM openjdk:17
COPY build/libs/loginapplication-0.0.1-SNAPSHOT.jar spring-loginapp.jar
CMD ["java", "-jar", "spring-loginapp.jar"]