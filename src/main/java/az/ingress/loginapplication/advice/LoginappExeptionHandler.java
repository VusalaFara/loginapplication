package az.ingress.loginapplication.advice;

import az.ingress.loginapplication.exception.UserNotFoundExeption;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class LoginappExeptionHandler {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String,String> invalidArgument(MethodArgumentNotValidException ex){
        Map<String,String>errorMap2 = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error -> {
            errorMap2.put(error.getField(),error.getDefaultMessage());
        });

        return errorMap2;
    }


    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(UserNotFoundExeption.class)
    public Map<String,String>handleUserNotFoundExeption(UserNotFoundExeption ex){
       Map<String,String>errorMap = new HashMap<>();
       errorMap.put("error message: ", ex.getMessage());
       return errorMap;
    }

}
