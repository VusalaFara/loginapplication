package az.ingress.loginapplication.controller;
import az.ingress.loginapplication.dto.CreateAccountRequest;
import az.ingress.loginapplication.dto.LoginRequest;
import az.ingress.loginapplication.exception.UserNotFoundExeption;
import az.ingress.loginapplication.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService service;

 @PostMapping("/create")
    public String createAccount(@Valid @RequestBody CreateAccountRequest request) {
        service.createAccount(request);
        return "User successfully registered!";
    }
    @PostMapping("/login")
    public String login(@Valid @RequestBody LoginRequest request) throws UserNotFoundExeption {
        service.login(request);
        return "successfully logged in!";
    }

    @PostMapping("/login2")
    public String login2(@Valid @RequestBody LoginRequest request) throws UserNotFoundExeption {
        service.login2(request);
        return "succesfully logged in!";
    }
}

