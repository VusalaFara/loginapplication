package az.ingress.loginapplication.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "login")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    Long id;
    @NotNull(message = "name shouldn't be null")
    @Column(name = "user_name")
    String name;
    @Email(message = "pleade add valid email address!")
    @Column(name = "user_email")
    String email;
    @Min(18)
    @Max(65)
    Integer age;
}
