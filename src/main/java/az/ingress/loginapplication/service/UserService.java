package az.ingress.loginapplication.service;
import az.ingress.loginapplication.dto.CreateAccountRequest;
import az.ingress.loginapplication.dto.LoginRequest;
import az.ingress.loginapplication.exception.UserNotFoundExeption;
import az.ingress.loginapplication.model.User;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;
import java.util.Optional;

public interface UserService {

    public Optional<User> findById(Long id);
    public List<User> getUser();
    public String saveUser(User user );
    public String updateUser(Long id, User user);
    public Long deleteUser(Long id);
    public String login(LoginRequest request) throws UserNotFoundExeption;
    public String login2(LoginRequest request)throws UserNotFoundExeption;

    String createAccount(CreateAccountRequest request);
}
