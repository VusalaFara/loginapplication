package az.ingress.loginapplication.service.impl;
import az.ingress.loginapplication.dto.CreateAccountRequest;
import az.ingress.loginapplication.dto.LoginRequest;
import az.ingress.loginapplication.exception.UserNotFoundExeption;
import az.ingress.loginapplication.model.User;
import az.ingress.loginapplication.repository.UserRepository;
import az.ingress.loginapplication.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final ModelMapper mapper;

    @Override
    public Optional<User> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<User> getUser() {
        return repository.findAll();
    }

    @Override
    public String saveUser(User user) {
         repository.save(user);
         return "saved";
    }

    @Override
    public String updateUser(Long id, User user) {
        if (repository.findById(id).isPresent()) {
            User user1 = repository.findById(id).get();
            user1.setName(user.getName());
            user1.setEmail(user.getEmail());
            user1.setAge(user.getAge());
            repository.save(user1);
        } else {
            throw new RuntimeException("user not found with : " + id);
        }
        return "added";
    }

    @Override
    public Long deleteUser(Long id) {
        repository.deleteById(id);
        return id;
    }


    @Override
    public String login(LoginRequest request) throws UserNotFoundExeption {
        repository.findAll().stream().filter(user ->
                        user.getName().equals(request.getUsername()) &&
                                user.getEmail().equals(request.getPassword())).findFirst().
                orElseThrow(() -> new UserNotFoundExeption("invalid username or password!"));
        return "logged in";
    }

    @Override
    public String login2(LoginRequest request) throws UserNotFoundExeption {
        if(repository.findByNameAndEmail(request.getUsername(),request.getPassword()).isPresent()){
            return "succesfully logged in";
        } else {
            throw  new UserNotFoundExeption("invalid username or password!");
        }
    }

    @Override
    public String createAccount(CreateAccountRequest request) {
        User user= mapper.map(request,User.class);
        repository.save(user);
        return "account created";
    }
}
